#include "access_manager.h"
#include "file_t.h"
#include <stdlib.h>

void free_buffer_dealloc(struct buffer_t * buf)
{
	free(buf->data);
	free(buf);
}

void free_buffer_void(struct buffer_t *)
{}

void release_buffer_dealloc(struct buffer_t * buf)
{
	free(buf);
}

void release_buffer_void(struct buffer_t *)
{}



void init_access_manager(struct access_manager_t * am, enum am_type type)
{
	am->type = type;
	switch(type)
	{
	case AM_MEM:
		am->fetch_data = fetch_data_MEM;
		am->put_data = put_data_MEM;
		am->open_file = open_file_MEM;
		am->close_file = close_file_MEM;
		am->allocate_space = allocate_space_MEM;
		am->free_buffer = free_buffer_void;			 // the buffer already was freed in release_buffer()
		am->release_buffer = release_buffer_dealloc; // buffer_t keeps a pointer to a place in the mem-buffer; the structure containing this pointer may be deallocated
		am->deallocate_space = deallocate_space_MEM;
		break;
	case AM_POSIX:
		am->fetch_data = fetch_data_POSIX;
		am->put_data = put_data_POSIX;
		am->open_file = open_file_POSIX;
		am->close_file = close_file_POSIX;
		am->allocate_space = allocate_space_POSIX;
		am->free_buffer = free_buffer_dealloc;	  // shall be done after syncing to file
		am->release_buffer = release_buffer_void; // mgr->{clean|dirty}_nodes keeps pointers to the buffers and they will be freed later
		am->deallocate_space = deallocate_space_POSIX;
		break;
	case AM_MMAP:
		printf("mmap");
		exit(1); // TODO
		break;
	case AM_MEM_MANAGED:
		am->fetch_data = fetch_data_MEM_MANAGED;
		am->put_data = put_data_MEM_MANAGED;
		am->open_file = open_file_MEM_MANAGED;
		am->close_file = close_file_MEM_MANAGED;
		am->allocate_space = allocate_space_MEM_MANAGED;
		am->free_buffer = free_buffer_void;			 // the buffer already was freed in release_buffer()
		am->release_buffer = release_buffer_dealloc; // buffer_t keeps a pointer to a place in the mem-buffer; the structure containing this pointer may be deallocated
		am->deallocate_space = deallocate_space_MEM_MANAGED;
		break;
	default:
		printf("am_default");
		exit(1); // TODO
	}
}
