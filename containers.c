#include "containers.h"



#define offset_parent 0
#define offset_left sizeof(ptr_type)
#define offset_right (offset_left + sizeof(ptr_type))
#define offset_balance (offset_right + sizeof(ptr_type))
#define offset_key (offset_balance + sizeof(int8_t))

void hexdump(unsigned char *ptr, int size);
void tree_rebalance_from_node(struct node_t * node, int8_t diff);
//void tree_rebalance_from_node_delete(struct node_t * node){};
bool search_key_pos(struct tree_t * tree, void * key, ptr_type * ret_pos, int * direction);
void tree_load_node(struct tree_t * tree, struct node_t * node, ptr_type pos);
ptr_type tree_successor_pos_of_node(struct node_t * source);
void tree_release_node(struct node_t * node);

void for_range_do(struct node_t * start,
	struct node_t * end,
	void (*fn)(struct node_t *, void *param),
	void * param);
void for_tree_do(struct tree_t * tree,
	void (*fn)(struct node_t *, void *param),
	void * param);

void rotate_double2(struct node_t * node, int diff);
void rotate_double1(struct node_t * node, int diff);
void rotate_left(struct node_t * node, int diff);
void rotate_right(struct node_t * node, int diff);



ptr_type sizeof_node(struct tree_t * tree)
{
	return tree->sizeof_key + tree->sizeof_value + offset_key;
}

ptr_type tree_leftmost(struct tree_t * tree)
{
	ptr_type iter_old = 0,
		iter = tree_get_root_pos(tree);
	while(iter != 0)
	{
		iter_old = iter;
		struct node_t node;
		tree_load_node(tree, &node, iter);
		iter = tree_get_left_pos(&node);
		tree_release_node(&node);
	}
	return iter_old;
}


ptr_type tree_rightmost(struct tree_t * tree)
{
	ptr_type iter_old = 0,
		iter = tree_get_root_pos(tree);
	while(iter != 0)
	{
		iter_old = iter;
		struct node_t node;
		tree_load_node(tree, &node, iter);
		iter = tree_get_right_pos(&node);
		tree_release_node(&node);
	}
	return iter_old;
}

struct buffer_t * fetch_buffer(struct buffer_manager_t * bm, ptr_type pos, ptr_type size)
{
	ptr_type key_pos = 0;
	int direction = 0;
	bool reloaded_dirty_buffer = false;
	struct buffer_t * buf, * tmp; // later on, buf and tmp shall point to allocated space in memory
	if(bm->clean_nodes != NULL && search_key_pos(bm->clean_nodes, (void *) &pos, &key_pos, &direction))
	{
		tmp = tree_fetch_node_buffer(bm->clean_nodes, key_pos);
		struct buffer_excerpt_t * be = create_buffer_excerpt(tmp, offset_key + bm->clean_nodes->sizeof_key, bm->clean_nodes->sizeof_value);
		bm->clean_nodes->value_extract(be, (void *) &buf, bm->clean_nodes->value_attrib);
		delete_buffer_excerpt(be);
		tmp->owner->access_manager->release_buffer(tmp);
	}
	else if(bm->dirty_nodes != NULL && search_key_pos(bm->dirty_nodes, (void *) &pos, &key_pos, &direction))
	{
		tmp = tree_fetch_node_buffer(bm->dirty_nodes, key_pos);
		struct buffer_excerpt_t * be = create_buffer_excerpt(tmp, offset_key + bm->dirty_nodes->sizeof_key, bm->dirty_nodes->sizeof_value);
		bm->dirty_nodes->value_extract(be, (void *) &buf, bm->dirty_nodes->value_attrib);
		delete_buffer_excerpt(be);
		tmp->owner->access_manager->release_buffer(tmp);
		reloaded_dirty_buffer = true;
	}
	else
	{
		buf = bm->access_manager->fetch_data(bm->storage, pos, size);
	}
	if((buf->owner != bm) || (size != buf->size))
	{
		printf("Error C1");
		abort();
	}
	if(!reloaded_dirty_buffer && (bm->access_manager->type != AM_MEM))
	{
		tree_insert_key_value(bm->clean_nodes, &pos, &buf);
	}
	return buf;
}


void free_buffer(struct buffer_t * buf)
{
	if(buf->owner->clean_nodes != NULL)
		tree_remove_key(buf->owner->clean_nodes, buf);
	if(buf->owner->dirty_nodes != NULL)
		tree_remove_key(buf->owner->dirty_nodes, buf);
	buf->owner->access_manager->free_buffer(buf);
}

void tree_set_key_type(struct tree_t * tree, enum val_type t, void * attrib)
{
	switch(t)
	{
	case val_uint64:
		tree->key_compare = uint64_compare;
		tree->key_inject = uint64_inject;
		tree->key_extract = uint64_extract;
		tree->sizeof_key = sizeof(uint64_t);
		tree->key_alloc = uint64_alloc;
		break;
	case val_uint32:
		tree->key_compare = uint32_compare;
		tree->key_inject = uint32_inject;
		tree->key_extract = uint32_extract;
		tree->sizeof_key = sizeof(uint32_t);
		tree->key_alloc = uint32_alloc;
		break;
	case val_null:
		tree->key_compare = null_compare;
		tree->key_inject = null_inject;
		tree->key_extract = null_extract;
		tree->sizeof_key = 0;
		tree->key_alloc = null_alloc;
		break;
	default:
		break;
	}
	tree->key_attrib = attrib;
}

void tree_set_value_type(struct tree_t * tree, enum val_type t, void * attrib)
{
	switch(t)
	{
	case val_uint64:
		tree->value_inject = uint64_inject;
		tree->value_extract = uint64_extract;
		tree->sizeof_value = sizeof(uint64_t);
		tree->value_alloc = uint64_alloc;
		break;
	case val_uint32:
		tree->value_inject = uint32_inject;
		tree->value_extract = uint32_extract;
		tree->sizeof_value = sizeof(uint32_t);
		tree->value_alloc = uint32_alloc;
		break;
	case val_null:
		tree->value_inject = null_inject;
		tree->value_extract = null_extract;
		tree->sizeof_value = 0;
		tree->value_alloc = null_alloc;
		break;
	default:
		break;
	}
	tree->value_attrib = attrib;
}

extern
void tree_flush(struct tree_t *tree)
{
	struct buffer_manager_t * bm = tree->buff_manager;
	struct tree_t * dn = bm->dirty_nodes;
	ptr_type iter = tree_leftmost(dn);
	int count = 0;
	while(iter != 0)
	{
		count++;
		struct node_t node;
		tree_load_node(dn, &node, iter);
		struct buffer_excerpt_t * be = create_buffer_excerpt(node.buffer, offset_key + dn->sizeof_key, sizeof(struct buffer_t *));
		struct buffer_t *tmp;
		dn->value_extract(be, &tmp, dn->value_attrib);
		bm->access_manager->put_data(bm->storage, tmp);
		delete_buffer_excerpt(be);
		
		be = create_buffer_excerpt(node.buffer, offset_key, dn->sizeof_key);
		ptr_type deletable;
		dn->key_extract(be, &deletable, dn->key_attrib);
		delete_buffer_excerpt(be);
		
		//deletable = iter;
		iter = tree_successor_pos_of_node(&node);
		
		tree_release_node(&node);
			printf("%x\n", deletable);
		if(tree_remove_key(bm->dirty_nodes, &deletable))
			printf("in dirty_nodes\n", deletable);
		if(tree_remove_key(bm->clean_nodes, &deletable))
			printf("in clean_nodes\n", deletable);
		// TODO: free node/buffer (not only release)
		tmp->owner->access_manager->free_buffer(tmp);
	}
	printf("%i flushed; finished part\n", count);
}

struct buffer_t * tree_fetch_node_buffer(struct tree_t * tree, ptr_type pos)
{
	return fetch_buffer(tree->buff_manager, pos, offset_key + tree->sizeof_key + tree->sizeof_value);
}


void tree_load_node(struct tree_t * tree, struct node_t * node, ptr_type pos)
{
	struct buffer_t * buf = tree_fetch_node_buffer(tree, pos);
	node->buffer = buf;
	node->owner = tree;
}

void tree_release_node(struct node_t * node)
{
	node->buffer->owner->access_manager->release_buffer(node->buffer);
}

ptr_type tree_get_root_pos(struct tree_t * tree)
{
	/*if(tree->buff_manager->access_manager->type == AM_MEM)
		return tree->pos_of_root_pos;*/
	struct buffer_t * root_pos_buf = fetch_buffer(tree->buff_manager, tree->pos_of_root_pos, sizeof(ptr_type));
	struct buffer_excerpt_t * be_root_pos = create_buffer_excerpt(root_pos_buf, 0, sizeof(ptr_type));
	ptr_type root_pos;
	tree->ptr_decode(be_root_pos, &root_pos, NULL);
	delete_buffer_excerpt(be_root_pos);
	root_pos_buf->owner->access_manager->release_buffer(root_pos_buf);
	//printf("root-pos: %i", root_pos);
	return root_pos;
}

void tree_set_root_pos(struct tree_t * tree, ptr_type pos)
{
	/*if(tree->buff_manager->access_manager->type == AM_MEM)
	{
		tree->pos_of_root_pos = pos;
		return;
	}*/
	struct buffer_t * root_pos_buf = fetch_buffer(tree->buff_manager, tree->pos_of_root_pos, sizeof(ptr_type));
	if(root_pos_buf == NULL)
	{
		//root_pos_buf = tree->buff_manager->access_manager->allocate_space(tree->buff_manager->storage, sizeof(ptr_type));
		printf("Error C2");
		exit(1);
	}
	struct buffer_excerpt_t * be_root_pos = create_buffer_excerpt(root_pos_buf, 0, sizeof(ptr_type));
	tree->ptr_encode(be_root_pos, &pos, NULL);
	delete_buffer_excerpt(be_root_pos);
	mark_dirty(root_pos_buf);
	root_pos_buf->owner->access_manager->release_buffer(root_pos_buf);
}


ptr_type tree_get_parent_pos(struct node_t * source_node)
{
	struct buffer_excerpt_t * be_parent_pos = create_buffer_excerpt(source_node->buffer, offset_parent, sizeof(ptr_type));
	ptr_type parent_pos;
	source_node->owner->ptr_decode(be_parent_pos, &parent_pos, NULL);
	delete_buffer_excerpt(be_parent_pos);
	return parent_pos;
}

ptr_type tree_get_right_pos(struct node_t * source_node)
{
	struct buffer_excerpt_t * be_right_pos = create_buffer_excerpt(source_node->buffer, offset_right, sizeof(ptr_type));
	ptr_type right_pos;
	source_node->owner->ptr_decode(be_right_pos, &right_pos, NULL);
	delete_buffer_excerpt(be_right_pos);
	return right_pos;
}
ptr_type tree_get_left_pos(struct node_t * source_node)
{
	struct buffer_excerpt_t * be_left_pos = create_buffer_excerpt(source_node->buffer, offset_left, sizeof(ptr_type));
	ptr_type left_pos;
	source_node->owner->ptr_decode(be_left_pos, &left_pos,  NULL);
	delete_buffer_excerpt(be_left_pos);
	return left_pos;
}

int8_t tree_get_node_balance(struct node_t * source_node)
{
	struct buffer_excerpt_t * be_balance = create_buffer_excerpt(source_node->buffer, offset_balance, sizeof(int8_t));
	int8_t balance;
	int8_extract((void*) be_balance, &balance, NULL);
	delete_buffer_excerpt(be_balance);
	return balance;
}





void tree_set_parent_pos(struct node_t * source_node, ptr_type pos)
{
	struct buffer_excerpt_t * be_parent_pos = create_buffer_excerpt(source_node->buffer, offset_parent, sizeof(ptr_type));
	source_node->owner->ptr_encode(be_parent_pos, &pos, NULL);
	delete_buffer_excerpt(be_parent_pos);
	mark_dirty(source_node->buffer);
}

void tree_set_right_pos(struct node_t * source_node, ptr_type pos)
{
	struct buffer_excerpt_t * be_right_pos = create_buffer_excerpt(source_node->buffer, offset_right, sizeof(ptr_type));
	source_node->owner->ptr_encode(be_right_pos, &pos, NULL);
	delete_buffer_excerpt(be_right_pos);
	mark_dirty(source_node->buffer);
}

void tree_set_left_pos(struct node_t * source_node, ptr_type pos)
{
	struct buffer_excerpt_t * be_left_pos = create_buffer_excerpt(source_node->buffer, offset_left, sizeof(ptr_type));
	source_node->owner->ptr_encode(be_left_pos, &pos,  NULL);
	delete_buffer_excerpt(be_left_pos);
	mark_dirty(source_node->buffer);
}

void tree_set_node_balance(struct node_t * source_node, int8_t balance)
{
	struct buffer_excerpt_t * be_balance_pos = create_buffer_excerpt(source_node->buffer, offset_balance, sizeof(int8_t));
	int8_inject(be_balance_pos, &balance, NULL);
	delete_buffer_excerpt(be_balance_pos);
	mark_dirty(source_node->buffer);
}






void tree_get_node_key_be(struct node_t * source_node, struct buffer_excerpt_t ** key)
{
	struct buffer_excerpt_t * be_key = create_buffer_excerpt(
		source_node->buffer,
		offset_key,
		source_node->owner->sizeof_key);
	
	*key = be_key;
}

void tree_get_node_value_be(struct node_t * source_node, struct buffer_excerpt_t ** value)
{
	struct buffer_excerpt_t * be_value = create_buffer_excerpt(
		source_node->buffer,
		offset_key + source_node->owner->sizeof_key,
		source_node->owner->sizeof_value);

	*value = be_value;
}



bool search_key_pos(struct tree_t * tree, void * key, ptr_type * ret_pos, int * direction)
{
	ptr_type p = tree_get_root_pos(tree);
	if(p == 0)
	{
		*ret_pos = 0;
		*direction = 0;
		return false;
	}
	struct node_t current_node;
	
	struct buffer_excerpt_t key_be;
	key_be.data = key;
	key_be.size = tree->sizeof_key;

	struct buffer_excerpt_t * current_key;
	
	*direction = 0;
	//tree_release_node(&current_node);
	ptr_type p_old = 0;
	int result = 0;
	do
	{
		tree_load_node(tree, &current_node, p);
		tree_get_node_key_be(&current_node, &current_key);
		result = tree->key_compare(&key_be, current_key, tree->key_attrib);
		delete_buffer_excerpt(current_key);
		
		p_old = p;
		if(result < 0)
		{
			p = tree_get_left_pos(&current_node);
			tree_release_node(&current_node);
			*direction = -1;
			if(p == 0)
			{
				*ret_pos = p_old;
				return false;
			}
		}
		else if(result > 0)// result > 0;
		{
			p = tree_get_right_pos(&current_node);
			tree_release_node(&current_node);
			*direction = 1;
			if(p == 0)
			{
				*ret_pos = p_old;
				return false;
			}
		}
		else
		{
			tree_release_node(&current_node);
		}
	}
	while(result != 0);
	*ret_pos = p;
	return true;
}

ptr_type tree_predecessor_pos_of_node(struct node_t *)
{
}

ptr_type tree_successor_pos_of_node(struct node_t * source)
{
	uint32_t val1, val2; // debug-data
	
	ptr_type pos, prev;
	struct tree_t *tree = source->owner;
	ptr_type right = tree_get_right_pos(source);
	if(right != 0)
	{
		pos = right;
		ptr_type old_pos = pos;
		while(pos != 0) // all the way left
		{
			old_pos = pos;
			struct node_t iterator;
			tree_load_node(tree, &iterator, pos);
			pos = tree_get_left_pos(&iterator);
			tree_release_node(&iterator);
			if(pos == right)
				break;
		}
		return old_pos;
	}
	else
	{
		bool go_on = true;
		prev = source->buffer->position;
		pos = tree_get_parent_pos(source);
		while(go_on && pos != 0)
		{
			struct node_t node;
			tree_load_node(tree, &node, pos);
			ptr_type tmp = tree_get_left_pos(&node);
			fflush(stdout);
			if(tmp == prev)
			{
				go_on = false;
			}
			else
			{
				prev = pos;
				pos = tree_get_parent_pos(&node);

			}
			tree_release_node(&node);
		}
	}
	return pos;
}

extern
bool tree_insert_key_value(struct tree_t * tree, void* key, void* value)
{
	ptr_type key_pos;
	int direction;
	if(search_key_pos(tree, key, &key_pos, &direction))
	{
		return false;
	}
	/*
	printf("tree: %p  ", tree);
	printf("trying to insert %x -> %lx\n", *((uint32_t*)key), *((uint64_t*)value));
	printf("key_pos: %i\n", key_pos);
	*/
	// initialize new node; will be released (in AM_MEM, it'll be freed) in ...->release_node() at the end of the function
	struct buffer_t * buf = tree->buff_manager->access_manager->allocate_space(tree->buff_manager->storage, sizeof_node(tree));
	if(tree->buff_manager->clean_nodes != NULL)
		tree_insert_key_value(tree->buff_manager->clean_nodes, &(buf->position), &buf);
	
	struct buffer_excerpt_t * be_key = create_buffer_excerpt(buf, offset_key, tree->sizeof_key);
	tree->key_inject(be_key, key, tree->key_attrib);
	
	delete_buffer_excerpt(be_key);
	
	struct buffer_excerpt_t * be_value = create_buffer_excerpt(buf, offset_key + tree->sizeof_key, tree->sizeof_value);
	tree->value_inject(be_value, value, tree->value_attrib);
	
	delete_buffer_excerpt(be_value);
	
	mark_dirty(buf);

	struct node_t node;
	node.owner = tree;
	node.buffer = buf;
	
	//hexdump(buf->data, sizeof_node(tree));
	
	tree_set_right_pos(&node, 0);
	tree_set_left_pos(&node, 0);
	tree_set_node_balance(&node, 0);
	
	// insert node into tree
	if(key_pos == 0)
	{
		tree_set_root_pos(tree, buf->position);
		tree_set_parent_pos(&node, 0);
	}
	else
	{
		struct node_t parent_node;
		tree_load_node(tree, &parent_node, key_pos);
		tree_set_parent_pos(&node, key_pos);
		struct buffer_excerpt_t *be1, *be2;
		tree_get_node_key_be(&parent_node, &be1);
		tree_get_node_key_be(&node, &be2);
		if(tree->key_compare(be2, be1, tree->key_attrib) >0)
		{
			tree_set_right_pos(&parent_node, buf->position);
			//tree_set_node_balance(&parent_node, tree_get_node_balance(&parent_node)+1);
			//printf("resulting balance: %i\n", tree_get_node_balance(&parent_node));
		}
		else if(tree->key_compare(be2, be1, tree->key_attrib) <0)
		{
			tree_set_left_pos(&parent_node, buf->position);
			//tree_set_node_balance(&parent_node, tree_get_node_balance(&parent_node)-1);
			//printf("resulting balance: %i\n", tree_get_node_balance(&parent_node));
		}
		else
		{
			abort();
		}
		tree_rebalance_from_node(&/*parent_*/node, 1);
		tree_release_node(&parent_node);
		delete_buffer_excerpt(be1);
		delete_buffer_excerpt(be2);
	}
	
	//printf("%02x: ", buf->position);
	//hexdump(node.buffer->data, sizeof_node(tree));
	
	tree_release_node(&node);
	//free_buffer(buf);
	return true;
}

extern
bool tree_remove_key(struct tree_t * tree, void* key)
{
	ptr_type key_pos;
	int direction;
	int8_t my_balance = 0;
	bool g_rebalance = false;
	bool found = search_key_pos(tree, key, &key_pos, &direction);
	if(!found)
	{
		return false;
	}
	
	struct node_t
		node = {0, 0},
		parent_node = {0, 0},
		successor_node = {0, 0},
		successor_parent_node = {0, 0};
	
	tree_load_node(tree, &node, key_pos);
	ptr_type
		left_child_pos = tree_get_left_pos(&node),
		right_child_pos = tree_get_right_pos(&node),
		parent_pos = tree_get_parent_pos(&node);
	
	bool skip = false;
	if(parent_pos != 0)
	{
		tree_load_node(tree, &parent_node, parent_pos);
		
		if(right_child_pos == 0 && left_child_pos != 0)
		{
			printf("a");
			struct node_t left_node;
			tree_load_node(tree, &left_node, left_child_pos);
			tree_set_parent_pos(&left_node, parent_pos);
			
			if(direction == -1)
			{
				tree_set_left_pos(&parent_node, left_child_pos);
				tree_set_node_balance(&parent_node, tree_get_node_balance(&parent_node)+1);
			}
			else if(direction == 1)
			{
				tree_set_right_pos(&parent_node, left_child_pos);
				tree_set_node_balance(&parent_node, tree_get_node_balance(&parent_node)-1);
			}
			skip = true;
			//tree_rebalance_from_node(&left_node, -1);
			tree_rebalance_from_node(&parent_node, -1);
		}
		else if(left_child_pos == 0 && right_child_pos != 0)
		{
			printf("b");
			struct node_t right_node;
			tree_load_node(tree, &right_node, right_child_pos);
			tree_set_parent_pos(&right_node, parent_pos);
			if(direction == -1)
			{
				tree_set_left_pos(&parent_node, right_child_pos);
				tree_set_node_balance(&parent_node, tree_get_node_balance(&parent_node)+1);
			}
			else if(direction == 1)
			{
				tree_set_right_pos(&parent_node, right_child_pos);
				tree_set_node_balance(&parent_node, tree_get_node_balance(&parent_node)-1);
			}
			skip = true;
			//tree_rebalance_from_node(&right_node, -1);
			if(tree_get_node_balance(&parent_node) != 1)
				tree_rebalance_from_node(&parent_node, -1);
		}
		else if(left_child_pos == 0 && right_child_pos == 0)
		{
			printf("c");
			if(direction == -1)
			{
				tree_set_left_pos(&parent_node, 0);
				tree_set_node_balance(&parent_node, tree_get_node_balance(&parent_node)+1);
			}
			else if(direction == 1)
			{
				tree_set_right_pos(&parent_node, 0);
				tree_set_node_balance(&parent_node, tree_get_node_balance(&parent_node)-1);
			}
			skip = true;
			if(tree_get_node_balance(&parent_node) != 1)
				tree_rebalance_from_node(&parent_node, -1);
		}
		// fetch successor and replace node with it; skip == false
	}
	else // parent_pos == 0; we're on root-pos
	{
		if(right_child_pos == 0)
		{
			printf("d");
			skip = true;
			if(direction == 0)
			{
				tree_set_root_pos(tree, left_child_pos);
			}
			else
			{
				abort();
			}

		}
		else if(left_child_pos == 0)
		{
			printf("e");
			skip = true;
			if(direction == 0)
			{
				tree_set_root_pos(tree, right_child_pos);
			}
			else
			{
				abort();
			}
		}
	}
	if(!skip)
	{
		ptr_type successor_pos = tree_successor_pos_of_node(&node);
		
		if(successor_pos != 0)
		{
			tree_load_node(tree, &successor_node, successor_pos);
			ptr_type successor_parent_pos = tree_get_parent_pos(&successor_node);
			if(successor_parent_pos == 0)
				abort();
			
			if(successor_parent_pos == key_pos)
			{
				printf("f");
				tree_set_parent_pos(&successor_node, parent_pos);
				tree_set_left_pos(&successor_node, left_child_pos);
				struct node_t left_node;
				tree_load_node(tree, &left_node, left_child_pos);
				tree_set_parent_pos(&left_node, successor_pos);
				//tree_set_node_balance(&successor_node, tree_get_node_balance(&successor_node)-1);
				tree_set_node_balance(&successor_node, tree_get_node_balance(&node)-1);
				if(direction == 1)
				{
					tree_set_right_pos(&parent_node, successor_pos);
				}
				else if(direction == -1)
				{
					tree_set_left_pos(&parent_node, successor_pos);
				}
				else
				{
					if(tree_get_root_pos(tree) == key_pos)
					{
						tree_set_root_pos(tree, successor_pos);
					}
					else
					{
						printf("Error C3\n");
						fflush(stdout);
						abort();
					}
				}
				struct node_t successor_parent_node;
				if(tree_get_node_balance(&successor_node) != -1)
					tree_rebalance_from_node(&successor_node, -1);
			}
			else
			{
				printf("g");
				tree_load_node(tree, &successor_parent_node, successor_parent_pos);
				ptr_type left = tree_get_left_pos(&successor_node);
				ptr_type right = tree_get_right_pos(&successor_node);
				
				my_balance = tree_get_node_balance(&successor_parent_node);
				tree_set_node_balance(&successor_parent_node, ++my_balance);
				if(left != 0)
				{
					printf("Error C4\n");
				}
				
				// adjust relations to right child of successor node
				struct node_t succ_right_child;
				tree_load_node(tree, &succ_right_child, right);
				tree_set_left_pos(&successor_parent_node, right);
				tree_set_parent_pos(&succ_right_child, successor_parent_pos);
				tree_release_node(&succ_right_child);
				
				struct node_t succ_left, succ_right;
				ptr_type left_pos, right_pos;
				left_pos = tree_get_left_pos(&node);
				right_pos = tree_get_right_pos(&node);
				tree_load_node(tree, &succ_left, left_pos);
				tree_load_node(tree, &succ_right, right_pos);
				tree_set_parent_pos(&successor_node, parent_pos);
				tree_set_left_pos(&successor_node, left_pos);
				tree_set_right_pos(&successor_node, right_pos);
				tree_set_parent_pos(&succ_left, successor_pos);
				tree_set_parent_pos(&succ_right, successor_pos);
				tree_release_node(&succ_left);
				tree_release_node(&succ_right);
				
				tree_set_node_balance(&successor_node,  tree_get_node_balance(&node));
				g_rebalance = true;
			}
		}
		if(parent_pos == 0)
		{
			tree_set_root_pos(tree, successor_pos);
		}
		else if(direction > 0) /* && !skip -- starts failing*/
		{
			tree_set_right_pos(&parent_node, successor_pos);
		}
		else if(direction < 0 & !skip)
		{
			tree_set_left_pos(&parent_node, successor_pos);
		}
	}
	
	
	
	node.buffer->owner->access_manager->deallocate_space(
		node.buffer->owner->storage,
		node.buffer);
	
	tree_release_node(&node);

	if(parent_node.owner != 0)
		tree_release_node(&parent_node);
	if(successor_node.owner != 0)
	{
		tree_release_node(&successor_node);
	}
	if(successor_parent_node.owner != 0)
	{
		if(my_balance != -1 && g_rebalance)
			tree_rebalance_from_node(&successor_parent_node, -1);
		tree_release_node(&successor_parent_node);
	}
	
	return true;
}

extern
bool tree_get_value_for_key(struct tree_t *tree, void * key, void ** value)
{
	ptr_type key_pos;
	int direction;
	bool found = search_key_pos(tree, key, &key_pos, &direction);
	if(!found)
	{
		printf("key %x not found  ", *((uint32_t*)key)); // debug-data
		*value = NULL;
		return false;
	}
	struct node_t node;
	tree_load_node(tree, &node, key_pos);
	
	struct buffer_excerpt_t * be_value;
	tree_get_node_value_be(&node, &be_value);
	
	void* return_value = tree->value_alloc(be_value, tree->value_attrib);
	tree->value_extract(be_value, return_value, tree->value_attrib);
		
	//printf("%i\n", *((uint32_t *) return_value)); //
	
	delete_buffer_excerpt(be_value);
	
	tree_release_node(&node);
	
	*value = return_value;
	return true;
}

void tree_rebalance_from_node(struct node_t * start, int8_t diff)
{
	struct node_t n1, n_;
	ptr_type
		p1 = 0,
		p_ = 0,
		next_pos = 0,
		prev_pos = 0;
	int8_t b1, b_;
	struct tree_t * tree = start->owner;
	p1 = start->buffer->position;
	
	bool fromleft = false;
	tree_load_node(tree, &n1, p1);
	
	
	b1 = tree_get_node_balance(&n1);
	if(diff < 0 && (b1 == -1 || b1 == 1))
		goto skip_loop;
	
	while(true)
	{
		b1 = tree_get_node_balance(&n1);
		
		next_pos = tree_get_parent_pos(&n1);
		
		if(prev_pos != 0)
		{
			if(prev_pos == tree_get_left_pos(&n1))
			{
				b1 -= diff;
				fromleft = true;
			}
			else if(prev_pos == tree_get_right_pos(&n1))
			{
				b1 += diff;
				fromleft = false;
			}
			else
			{
				printf("Error C5\n");
				fflush(stdout);
				abort();
			}
			tree_set_node_balance(&n1, b1);
		}
		
		if(b1 == 2)
		{
			p_ = tree_get_right_pos(&n1);
			tree_load_node(n1.owner, &n_, p_);
			b_ = tree_get_node_balance(&n_);
			tree_release_node(&n_);
			if(b_ == 1|| b_ == 0)
			{
				printf("l");
				rotate_left(&n1, diff);
				if(diff > 0)
					next_pos = 0;
				else
				{
					p1 = tree_get_parent_pos(&n1);
					tree_release_node(&n1);
					tree_load_node(n1.owner, &n1, p1);
					if(tree_get_node_balance(&n1) == -1)
					{
						next_pos = 0;
					}
					else
					{
						next_pos = tree_get_parent_pos(&n1);
					}
				}
			}
			else if(b_ == -1 )
			{
				printf("2");
				rotate_double2(&n1, diff);
				if(diff > 0)
					next_pos = 0;
				else
				{
					p1 = tree_get_parent_pos(&n1);
					tree_release_node(&n1);
					tree_load_node(n1.owner, &n1, p1);
					next_pos = tree_get_parent_pos(&n1);
					p1 = n1.buffer->position;
				}
			}
			else
			{
				printf("Error C6\n");
				fflush(stdout);
				abort();
			}
		}
		else if(b1 == -2)
		{
			p_ = tree_get_left_pos(&n1);
			tree_load_node(n1.owner, &n_, p_);
			b_ = tree_get_node_balance(&n_);
			tree_release_node(&n_);
			if(b_ == -1 || b_ == 0)
			{
				printf("r");
				rotate_right(&n1, diff);
				if(diff > 0)
					next_pos = 0;
				else
				{
					p1 = tree_get_parent_pos(&n1);
					tree_release_node(&n1);
					tree_load_node(n1.owner, &n1, p1);
					if(tree_get_node_balance(&n1) == 1)
					{
						next_pos = 0;
					}
					else
					{
						next_pos = tree_get_parent_pos(&n1);
					}
				}
			}
			else if(b_ == 1)
			{
				printf("1");
				rotate_double1(&n1, diff);
				if(diff > 0)
					next_pos = 0;
				else
				{
					p1 = tree_get_parent_pos(&n1);
					tree_release_node(&n1);
					tree_load_node(n1.owner, &n1, p1);
					next_pos = tree_get_parent_pos(&n1);
					printf("next %x, prev %x\n", next_pos, p1);
					p1 = n1.buffer->position;
				}
			}
			else
			{
				printf("Error C7\n");
				fflush(stdout);
				abort();
			}
		}
		
		
		if(next_pos == 0)
			break;
		if(prev_pos != 0)
		{
			if(diff == 1)
			{
				if(b1 == 0)
					break;
			}
			else
			{
				if(fromleft && b1 == 1)
					break;
				else if(!fromleft && b1 == -1)
					break;
			}
		}
		// prepare next iteration
		prev_pos = p1;
		p1 = next_pos;
		tree_release_node(&n1);
		tree_load_node(tree, &n1, p1);
	}
skip_loop:
	tree_release_node(&n1);
	printf("\n");
}



void mark_dirty(struct buffer_t* buff)
{
	struct buffer_manager_t *bm = buff->owner;
	if(bm->access_manager->type == AM_MEM)
		return;
	ptr_type pos;
	int direction;

	if(search_key_pos(bm->clean_nodes, (void*) &(buff->position), &pos, &direction))
	{
		tree_remove_key(bm->clean_nodes,
			(void*) &(buff->position));
		tree_insert_key_value(bm->dirty_nodes,
			(void*) &(buff->position),
			&buff);
	}
}

extern
void init_buffer_manager(struct buffer_manager_t * mgr,
	struct access_manager_t * am_main,
	char * filename,
	struct access_manager_t * am_mem)
{
	mgr->access_manager = am_main;
	if(am_mem->type != AM_MEM)
	{
		printf("Error C8");
		exit(1); // TODO
	}
	mgr->storage = am_main->open_file(filename, mgr);
	if(am_main->type != AM_MEM)
	{
		struct buffer_manager_t * mem_bm = malloc(sizeof(struct buffer_manager_t));
		init_buffer_manager(mem_bm, am_mem, "", am_mem);
		mgr->clean_nodes = malloc(sizeof(struct tree_t));
		mgr->dirty_nodes = malloc(sizeof(struct tree_t));
		create_tree(mgr->clean_nodes, sizeof(ptr_type), ptr_type_enum, NULL, val_uint64, NULL, mem_bm);
		tree_set_root_pos(mgr->clean_nodes, 0);
		create_tree(mgr->dirty_nodes, sizeof(ptr_type) *2, ptr_type_enum, NULL, val_uint64, NULL, mem_bm);
		tree_set_root_pos(mgr->dirty_nodes, 0);
	}
	else
	{
		mgr->clean_nodes = NULL;
		mgr->dirty_nodes = NULL;
	}
}

void wipe_bm_tree(struct tree_t *tree, struct tree_t * mgd_tree)
{
	ptr_type
		next = tree_get_root_pos(tree),
		current = 0;
	struct node_t iter, iter_c;
	int depth = 1;
	
	if(next == 0)
		return;
	
	while(next != 0)
	{
		tree_load_node(tree, &iter, next);
		current = next;
		next = tree_get_left_pos(&iter);
		tree_release_node(&iter);
		depth++;
	}
	struct buffer_t ** buffer_stack = malloc(sizeof(struct buffer_t *) * 2*(depth));
	int depth_counter = 0;
	ptr_type root_pos = tree_get_root_pos(tree);
	
	/*next = tree_get_root_pos(tree),
	while(next != 0)
	{
		node_stack[depth_counter++] = next;
		tree_load_node(tree, &iter, next);
		current = next;
		next = tree_get_left_pos(&iter);
		tree_release_node(&iter);
	}
	tree_load_node(tree, &iter, current);*/
	tree_load_node(tree, &iter, root_pos);
	
	struct buffer_t * next_buf = tree->buff_manager->access_manager->fetch_data(
				tree->buff_manager->storage,
				root_pos,
				sizeof_node(tree));
	
	iter.owner = tree;
	iter.buffer = next_buf;
	buffer_stack[depth_counter] = iter.buffer;
	depth_counter++;
	
	int counter = 0;
	int times_visited_0 = 0;
	
	struct node_t local_iter = iter;
	while(true)
	{
		// don't try this:
		// current = tree_successor_pos_of_node(&iter);
		// It leads to defective trees, resulting in a memory-leak.
		

		
		ptr_type
			left = tree_get_left_pos(&local_iter),
			parent = tree_get_parent_pos(&local_iter),
			right = tree_get_right_pos(&local_iter),
			pos = local_iter.buffer->position;
			
		if(left != 0)
		{
			next_buf = tree->buff_manager->access_manager->fetch_data(
				tree->buff_manager->storage,
				left,
				sizeof_node(tree));
			tree_set_left_pos(&local_iter, 0);
			buffer_stack[depth_counter] = next_buf;
			local_iter.owner = tree;
			local_iter.buffer = next_buf;
			printf("l %x; ", next_buf->position);
			depth_counter++;
		}
		else if(right != 0)
		{
			next_buf = tree->buff_manager->access_manager->fetch_data(
				tree->buff_manager->storage,
				right,
				sizeof_node(tree));
			tree_set_right_pos(&local_iter, 0);
			buffer_stack[depth_counter] = next_buf;
			local_iter.owner = tree;
			local_iter.buffer = next_buf;
			printf("r %x; ", next_buf->position);
			depth_counter++;
		}
		else
		{
			printf("u\n");
			fflush(stdout);
			// retrieve managed data
			//struct node_t *ptr = NULL;
			struct buffer_t *ptr = NULL;
			struct buffer_t *b = local_iter.buffer;
			
			struct buffer_excerpt_t *ptr_be = create_buffer_excerpt(
				b,
				offset_key + tree->sizeof_key,
				tree->sizeof_value);
			tree->value_extract(ptr_be, &ptr, tree->value_attrib);
			delete_buffer_excerpt(ptr_be);
			
			bool test = ((parent == 0) && right == 0)
				|| (parent == pos);
			//hexdump((unsigned char *) ptr, sizeof(struct node_t));
			printf("%p\n", ptr);
			printf("%p\n", ptr->owner);
			printf("%i\n", ptr->size);
			
			ptr->owner->/*buff_manager->*/access_manager->free_buffer(ptr/*->buffer*/);
			depth_counter--;
			printf("%i: %i %x %x\t %x %x\n", counter, depth_counter, left, right, pos, root_pos);
			fflush(stdout);
			b->owner->access_manager->free_buffer(b);
			--depth_counter;
			if(depth_counter < 1)
				times_visited_0++;
			if(depth_counter < 1 && times_visited_0 == 2)
			{
				printf("counter: %i\n", counter);
				return;
			}
			local_iter.buffer = buffer_stack[depth_counter];
			
			counter++;
		}
		
	}
	
}

extern
void delete_buffer_manager(struct buffer_manager_t *mgr, struct tree_t * mgd_tree)
{
	wipe_bm_tree(mgr->dirty_nodes, mgd_tree);
	wipe_bm_tree(mgr->clean_nodes, mgd_tree);
	
	
	if(mgr != NULL)
	{
		mgr->clean_nodes->buff_manager->access_manager->close_file(mgr->clean_nodes->buff_manager->storage); //release RAM
		free(mgr->clean_nodes->buff_manager); // not delete_buffermanager: this part is hidden to the user
		if(mgr->clean_nodes != NULL)
			free(mgr->clean_nodes);
		if(mgr->dirty_nodes != NULL)
			free(mgr->dirty_nodes);
		
		// dirty_nodes uses the same manager and the same storage
			// so it's not necessary to free them, too.
		mgr->access_manager->close_file(mgr->storage);
	}
}

extern
void create_tree(
	struct tree_t * tree,
	ptr_type pos,
	enum val_type key_t, void * key_attrib,
	enum val_type val_t, void * val_attrib,
	struct buffer_manager_t * bm_file)
{
	switch(sizeof(ptr_type))
	{
	case 1:
		printf("Error C9");
		exit(1);
		break;
	case 2:
		printf("Error CA");
		exit(1);
		break;
	case 4:
		tree->ptr_decode = uint32_extract;
		tree->ptr_encode = uint32_inject;
		break;
	case 8:
		tree->ptr_decode = uint64_extract;
		tree->ptr_encode = uint64_inject;
		break;
	default:
		printf("Error CB");
		exit(1);
	}
	tree->pos_of_root_pos = pos;
	tree->buff_manager = bm_file;
	tree_set_key_type(tree, key_t, key_attrib);
	tree_set_value_type(tree, val_t, val_attrib);
	if(tree->buff_manager->storage->size < pos + sizeof(ptr_type))
	{
		struct buffer_t * new_buf = tree->buff_manager->access_manager->allocate_space(
			tree->buff_manager->storage,
			pos + sizeof(ptr_type) - tree->buff_manager->storage->size);
		tree->buff_manager->access_manager->release_buffer(new_buf);
		tree->buff_manager->access_manager->free_buffer(new_buf);
	}
}

extern
void delete_tree(struct tree_t * tree)
{
	// trees may share a buffer_manager_t, so don't do this:
	//delete_buffer_manager(tree->buff_manager); 
}

void rotate_left(struct node_t * node, int diff)
{
	struct node_t x, y, B, parent;
	ptr_type x_pos, y_pos, parent_pos, B_pos;
	x = *node;
	x_pos = x.buffer->position;
	y_pos = tree_get_right_pos(&x);
	if(y_pos == 0)
	{
		printf("Error CC\n");
		abort();
	}
	tree_load_node(x.owner, &y, y_pos);
	B_pos = tree_get_left_pos(&y);
	if(B_pos != 0)
		tree_load_node(y.owner, &B, B_pos);
	
	parent_pos = tree_get_parent_pos(&x);

	tree_set_right_pos(&x, B_pos);
	tree_set_left_pos(&y, x_pos);
	tree_set_parent_pos(&y, parent_pos);
	tree_set_parent_pos(&x, y.buffer->position);
	if(B_pos != 0)
		tree_set_parent_pos(&B, x_pos);
	int8_t
		b_y = tree_get_node_balance(&y),
		b_x = tree_get_node_balance(&x);
	if(diff > 0)
	{
		if(b_y == 1) 
		{
			tree_set_node_balance(&x, 0);
			tree_set_node_balance(&y, 0);
		}
	}
	else
	{
		printf("\np%x b%i, p%x, b%i\n", y_pos, b_y, x_pos, b_x);
		if(b_y == 1) 
		{
			tree_set_node_balance(&x, 0);
			tree_set_node_balance(&y, 0);
		}
		else if(b_y == -1)
		{
			tree_set_node_balance(&x, 1);
			tree_set_node_balance(&y, -1);
		}
		else if(b_y == 2)
		{
			tree_set_node_balance(&x, -1);
			tree_set_node_balance(&y, -1);
		}
		else if(b_y == -2) // TODO
		{
			tree_set_node_balance(&x, 1);
			tree_set_node_balance(&y, 1);
		}
		else if(b_y == 0)
		{
			tree_set_node_balance(&x, 1);
			tree_set_node_balance(&y, -1);
		}
	}
	
	if(parent_pos == 0)
	{
		tree_set_root_pos(y.owner, y.buffer->position);
	}
	else
	{
		tree_load_node(y.owner, &parent, parent_pos);
		if(tree_get_left_pos(&parent) == x_pos)
		{
			tree_set_left_pos(&parent, y.buffer->position);
		}
		else if(tree_get_right_pos(&parent) == x_pos)
		{
			tree_set_right_pos(&parent, y.buffer->position);
		}
		else
		{
			printf("Error CD\n");
			fflush(stdout);
			abort();
		}

		tree_release_node(&parent);
	}

	//tree_release_node(&x);
	tree_release_node(&y);
	if(B_pos != 0)
		tree_release_node(&B);
}


void rotate_right(struct node_t * node, int diff)
{
	struct node_t x, y, B, parent;
	x = *node;
	ptr_type x_pos = x.buffer->position,
		parent_pos, B_pos, y_pos = tree_get_left_pos(&x);
	if(y_pos == 0)
	{
		printf("Error CE\n");
		dump_tree(node->owner, 1500);
		abort();
	}
	tree_load_node(x.owner, &y, y_pos);
	parent_pos = tree_get_parent_pos(&x);

	if(tree_get_left_pos(&x) == y_pos)
	{
			B_pos = tree_get_right_pos(&y);
			if(B_pos != 0)
				tree_load_node(y.owner, &B, B_pos);
			
			tree_set_parent_pos(&y, parent_pos);
			tree_set_right_pos(&y, x_pos);
			tree_set_parent_pos(&x, y_pos);
			tree_set_left_pos(&x, B_pos);
		}
		else
		{
			B_pos = tree_get_right_pos(&y);
			if(B_pos != 0)
				tree_load_node(y.owner, &B, B_pos);
			
			tree_set_parent_pos(&y, parent_pos);
			tree_set_right_pos(&y, x_pos);
			tree_set_parent_pos(&x, y_pos);
			tree_set_left_pos(&x, B_pos);
		}
	}
	/*else if(tree_get_right_pos(&x) == y_pos)
	{
		B_pos = tree_get_left_pos(&y);
		if(B_pos != 0)
			tree_load_node(y.owner, &B, B_pos);
		
		tree_set_parent_pos(&y, parent_pos);
		tree_set_left_pos(&y, x_pos);
		tree_set_parent_pos(&x, y_pos);
		tree_set_right_pos(&x, B_pos);
	}*/
	else
	{
		printf("Error CF\n");
		fflush(stdout);
		abort();
	}
	if(B_pos != 0)
		tree_set_parent_pos(&B, x_pos);


	int8_t b_y = tree_get_node_balance(&y);
		printf("\np%x b%i\n", y_pos, b_y);
	if(b_y == -1) 
	{
		tree_set_node_balance(&x, 0);
		tree_set_node_balance(&y, 0);
	}
	else if(b_y == 1) 
	{
		tree_set_node_balance(&x, -1);
		tree_set_node_balance(&y, 2);
	}
	else if(b_y == 2)
	{
		tree_set_node_balance(&x, -1);
		tree_set_node_balance(&y, -1);
	}
	else if(b_y == -2) // TODO
	{
		tree_set_node_balance(&x, 0);
		tree_set_node_balance(&y, 0);
	}
	else if(b_y == 0)
	{
		tree_set_node_balance(&x, -1);
		tree_set_node_balance(&y, 1);
	}

	if(parent_pos == 0)
	{
		tree_set_root_pos(y.owner, y_pos);
	}
	else
	{
		tree_load_node(y.owner, &parent, parent_pos);
		if(tree_get_left_pos(&parent) == x.buffer->position)
		{
			tree_set_left_pos(&parent, y_pos);
		}
		else if(tree_get_right_pos(&parent) == x.buffer->position)
		{
			tree_set_right_pos(&parent, y_pos);
		}
		else
		{
			printf("Error CF\n");
			fflush(stdout);
			abort();
		}
		tree_release_node(&parent);
	}
	//tree_release_node(&x);
	tree_release_node(&y);
	if(B_pos != 0)
		tree_release_node(&B);
}

void rotate_double1(struct node_t * node, int diff)
{
	bool is_remove = (diff < 0);
	struct node_t x, y, z, B1, B2, parent;
	ptr_type x_pos, y_pos, z_pos, parent_pos, B1_pos, B2_pos;
	z = *node;
	z_pos = z.buffer->position;
	x_pos = tree_get_left_pos(&z);
	if(x_pos == 0)
		abort();
	tree_load_node(z.owner, &x, x_pos);
	y_pos = tree_get_right_pos(&x);
	if(y_pos == 0)
		abort();
	tree_load_node(z.owner, &y, y_pos);
	B1_pos = tree_get_left_pos(&y);
	B2_pos = tree_get_right_pos(&y);
	if(B1_pos != 0)
		tree_load_node(y.owner, &B1, B1_pos);
	if(B2_pos != 0)
		tree_load_node(y.owner, &B2, B2_pos);
	parent_pos = tree_get_parent_pos(&z);

	tree_set_right_pos(&x, B1_pos);
	tree_set_parent_pos(&x, y_pos);
	if(B1_pos != 0)
		tree_set_parent_pos(&B1, x_pos);
	tree_set_left_pos(&y, x_pos);
	tree_set_right_pos(&y, z_pos);
	tree_set_parent_pos(&y, parent_pos);
	tree_set_left_pos(&z, B2_pos);
	tree_set_parent_pos(&z, y_pos);
	if(B2_pos != 0)
		tree_set_parent_pos(&B2, z_pos);

	int8_t b_y = tree_get_node_balance(&y);
	printf("\n%x %i\n", y_pos, b_y);
	if(b_y == -1)
	{
		if(is_remove)
		{
			tree_set_node_balance(&x, 0);
			tree_set_node_balance(&z, 1);
		}
		else
		{
			tree_set_node_balance(&x, 0);
			tree_set_node_balance(&z, 1);
		}
	}
	else if (b_y == 1)
	{
		if(is_remove)
		{
			tree_set_node_balance(&x, 0);
			tree_set_node_balance(&z, 1);
		}
		else
		{
			tree_set_node_balance(&x, -1);
			tree_set_node_balance(&z, 0);
		}
	}
	else
	{
		if(is_remove)
		{
			tree_set_node_balance(&x, -1);
			tree_set_node_balance(&z, 0);
		}
		else
		{
			tree_set_node_balance(&x, 0);
			tree_set_node_balance(&z, 0);
		}
	}
	
	if(is_remove)
		tree_set_node_balance(&y, 0);
	else
		tree_set_node_balance(&y, 0);

	if(parent_pos == 0)
	{
		tree_set_root_pos(y.owner, y_pos);
	}
	else
	{
		tree_load_node(y.owner, &parent, parent_pos);
		if(tree_get_left_pos(&parent) == z_pos)
		{
			tree_set_left_pos(&parent, y_pos);
		}
		else if(tree_get_right_pos(&parent) == z_pos)
		{
			tree_set_right_pos(&parent, y_pos);
		}
		else
		{
			printf("Error C10");
			fflush(stdout);
			abort();
		}
		/*tree_set_node_balance(&parent,
			tree_get_node_balance(&parent)-1);*/
		tree_release_node(&parent);
	}

	tree_release_node(&x);
	tree_release_node(&y);
	//tree_release_node(&z);
	if(B1_pos != 0)
		tree_release_node(&B1);
	if(B2_pos != 0)
		tree_release_node(&B2);
}

void rotate_double2(struct node_t * node, int diff)
{
	bool is_remove= false;
	struct node_t x, y, z, B1, B2, parent;
	ptr_type x_pos, y_pos, z_pos, parent_pos, B1_pos, B2_pos;
	x = *node;
	x_pos = x.buffer->position;
	z_pos = tree_get_right_pos(&x);
	if(z_pos == 0)
		abort();
	tree_load_node(x.owner, &z, z_pos);
	y_pos = tree_get_left_pos(&z);
	if(y_pos == 0)
	{
		printf("x %x y %x z %x\n", x_pos, y_pos, z_pos);
		dump_tree(node->owner, 5000);
		abort();
	}
	tree_load_node(x.owner, &y, y_pos);
	B1_pos = tree_get_left_pos(&y);
	B2_pos = tree_get_right_pos(&y);
	if(B1_pos != 0)
		tree_load_node(y.owner, &B1, B1_pos);
	if(B2_pos != 0)
		tree_load_node(y.owner, &B2, B2_pos);
	parent_pos = tree_get_parent_pos(&x);

	tree_set_right_pos(&x, B1_pos);
	if(B1_pos != 0)
		tree_set_parent_pos(&B1, x_pos);
	tree_set_parent_pos(&z, y_pos);
	tree_set_left_pos(&y, x_pos);
	tree_set_right_pos(&y, z_pos);
	tree_set_parent_pos(&y, parent_pos);
	tree_set_parent_pos(&x, y_pos);
	if(B2_pos != 0)
		tree_set_parent_pos(&B2, z_pos);
	tree_set_left_pos(&z, B2_pos);

	int8_t
		b_y = tree_get_node_balance(&y),
		b_x = tree_get_node_balance(&x),
		b_z = tree_get_node_balance(&z);
	if(diff < 0)
	{
		printf("\n%x %i\n", x_pos, b_x);
		printf("\n%x %i\n", y_pos, b_y);
		printf("\n%x %i\n", z_pos, b_z);
		if(b_y == -1)
		{
			tree_set_node_balance(&x, 0);
			tree_set_node_balance(&z, 1);
		}
		else if (b_y == 1)
		{
			tree_set_node_balance(&x, -1);
			tree_set_node_balance(&z, 0);
		}
		else
		{
			tree_set_node_balance(&x, 0);
			tree_set_node_balance(&z, 0);
		}
	}
	else
	{
		if(b_y == -1)
		{
			tree_set_node_balance(&x, 0);
			tree_set_node_balance(&z, 1);
		}
		else if (b_y == 1)
		{
			tree_set_node_balance(&x, -1);
			tree_set_node_balance(&z, 0);
		}
		else
		{
			tree_set_node_balance(&x, 0);
			tree_set_node_balance(&z, 0);
		}
	}

	tree_set_node_balance(&y, 0);
	
	if(parent_pos == 0)
	{
		tree_set_root_pos(y.owner, y_pos);
	}
	else
	{
		tree_load_node(y.owner, &parent, parent_pos);
		if(tree_get_left_pos(&parent) == x.buffer->position)
		{
			tree_set_left_pos(&parent, y_pos);
		}
		else if(tree_get_right_pos(&parent) == x.buffer->position)
		{
			tree_set_right_pos(&parent, y_pos);
		}
		else
		{
			printf("Error C11");
			fflush(stdout);
			abort();
		}
		/*tree_set_node_balance(&parent,
			tree_get_node_balance(&parent)-1);*/
		tree_release_node(&parent);
	}

	//tree_release_node(&x);
	tree_release_node(&y);
	tree_release_node(&z);
	if(B1_pos != 0)
		tree_release_node(&B1);
	if(B2_pos != 0)
		tree_release_node(&B2);
}

void dump_subtree(struct node_t * node, FILE * fp);
void dump_subtree(struct node_t * node, FILE * fp)
{
	struct node_t left, right;
	struct tree_t *tree = node->owner;
	ptr_type left_p, right_p;
	left_p = tree_get_left_pos(node);
	right_p = tree_get_right_pos(node);
	
	struct buffer_excerpt_t * be;
	tree_get_node_key_be(node, &be);
	
	uint32_t t;
	tree->key_extract(be, &t, tree->key_attrib);
	
	delete_buffer_excerpt(be);
	
	fprintf(fp, "x%x [label=\"%x, %x, %i\"]\nx%x -> x%x [label=\"p\"]\n",
		node->buffer->position, node->buffer->position,
		t , tree_get_node_balance(node),
		node->buffer->position, tree_get_parent_pos(node));
	if(left_p != 0)
	{
		tree_load_node(tree, &left, left_p);
	}
	if(right_p != 0)
	{
		tree_load_node(tree, &right, right_p);
	}
	
	//printf("%02x (%02x): %02x, %02x\n", node->buffer->position, t, left_p, right_p);
	
	//hexdump(node->buffer->data, sizeof_node(tree));
	if(left_p != 0)
	{
		fprintf(fp, "x%x -> x%x [label=\"l\"]\n",
			node->buffer->position, left_p);
		dump_subtree(&left, fp);
	}
	if(right_p != 0)
	{
		fprintf(fp, "x%x -> x%x[label=\"r\"]\n",
			node->buffer->position, right_p);
		dump_subtree(&right, fp);
	}
}

void dump_tree(struct tree_t * tree, int n)
{
	struct node_t start;
	/*
	printf("\ndirty_nodes:\n");
	{
		char buf[1024];
		snprintf(buf, 1024, "%05i_dntree.dot", n);
		FILE * fp = fopen(buf, "w");
		if(tree->buff_manager->dirty_nodes != 0)
		{
			fprintf(fp, "digraph G {");
			ptr_type pos = tree_get_root_pos(tree->buff_manager->dirty_nodes);
			printf("root pos: %x\n", pos);
			if(pos != 0)
			{
				tree_load_node(tree->buff_manager->dirty_nodes, &start, pos);
				dump_subtree(&start, fp);
			}
			fprintf(fp, "}");
		}
		fclose(fp);
	}*/
	/*
	printf("\nclean_nodes:\n");
	{
		char buf[1024];
		snprintf(buf, 1024, "%05i_cntree.dot", n);
		FILE * fp = fopen(buf, "w");
		if(tree->buff_manager->clean_nodes != 0)
		{
			fprintf(fp, "digraph G {");
			ptr_type pos = tree_get_root_pos(tree->buff_manager->clean_nodes);
			printf("root pos: %x\n", pos);
			if(pos != 0)
			{
				tree_load_node(tree->buff_manager->clean_nodes, &start, pos);
				dump_subtree(&start, fp);
			}
			fprintf(fp, "}");
		}
		fclose(fp);
	}
	*/
	printf("\ntree %i:\n", n	);
	{
		char buf[1024];
		snprintf(buf, 1024, "%05i_tree.dot", n);
		FILE * fp = fopen(buf, "w");
		fprintf(fp, "digraph G {");
		ptr_type pos = tree_get_root_pos(tree);
		printf("root pos: %x\n", pos);
		if(pos != 0)
		{
			tree_load_node(tree, &start, pos);
			dump_subtree(&start, fp);
		}
		fprintf(fp, "}");
		fclose(fp);
	}
}

void hexdump(unsigned char *ptr, int size)
{
	printf("%p(%i) ", ptr, size);
	for(unsigned char * i = ptr; i < ptr+size; i++)
	{
		printf(" %02x", *i);
	}
	printf("\n");
}

int test_node_avl(struct node_t * node);
int test_node_avl(struct node_t * node)
{
	struct node_t left, right;
	struct tree_t *tree = node->owner;
	ptr_type left_p, right_p;
	left_p = tree_get_left_pos(node);
	right_p = tree_get_right_pos(node);
	
	struct buffer_excerpt_t * be;
	tree_get_node_key_be(node, &be);
	
	uint32_t t;
	int8_t b1 = 0, b2 = 0, b = 0;
	
	tree->key_extract(be, &t, tree->key_attrib);
	
	delete_buffer_excerpt(be);
	b = tree_get_node_balance(node);
	
	if(left_p != 0)
	{
		tree_load_node(tree, &left, left_p);
		b1 = test_node_avl(&left);
		tree_release_node(&left);
	}
	if(right_p != 0)
	{
		tree_load_node(tree, &right, right_p);
		b2 = test_node_avl(&right);
		tree_release_node(&right);
	}

	
	int bal = (b1 > b2)?b1:b2;
	
	if(b == b2-b1 && b > -2 && b < 2)
	{
		return bal+1;
	}
	else
	{
		printf("pos: %x key: %x, dl %i, dr %i, b %i, depth %i\n", node->buffer->position, t, b1, b2, b, bal);
		fflush(stdout);
		dump_tree(node->owner, 1400);
		abort();
	}
}

void test_avl(struct tree_t * tree)
{
	struct node_t start;
	{
		if(tree->buff_manager->dirty_nodes != 0)
		{
			ptr_type pos = tree_get_root_pos(tree->buff_manager->dirty_nodes);
			if(pos != 0)
			{
				tree_load_node(tree->buff_manager->dirty_nodes, &start, pos);
				test_node_avl(&start);
				tree_release_node(&start);
			}
		}
	}
	{
		if(tree->buff_manager->clean_nodes != 0)
		{
			ptr_type pos = tree_get_root_pos(tree->buff_manager->clean_nodes);
			if(pos != 0)
			{
				tree_load_node(tree->buff_manager->clean_nodes, &start, pos);
				test_node_avl(&start);
				tree_release_node(&start);
			}
		}
	}
	{
		ptr_type pos = tree_get_root_pos(tree);
		if(pos != 0)
		{
			tree_load_node(tree, &start, pos);
			test_node_avl(&start);
			tree_release_node(&start);
		}
	}
}



void for_range_do(struct node_t * start, struct node_t * end,
	void (*fn)(struct node_t *node, void *param),
	void * param)
{
	struct tree_t* tree;
	if(start != NULL)
		tree = start->owner;
	else if(end != NULL)
		tree = end->owner;
	else
	{
		printf("In for_range_do(), start and end are NULL.\n");
		abort();
	}
	
	ptr_type new_pos = -1;
	
	struct node_t iter;
		
	if(start == NULL)
		tree_load_node(tree, &iter, tree_leftmost(tree));
	else
		iter = *start;
	while(new_pos != 0 || (end != NULL && (iter.buffer->position != end->buffer->position)))
	{
		fn(&iter, param);
		new_pos = tree_successor_pos_of_node(&iter);
		tree_release_node(&iter);
		if(new_pos != 0)
			tree_load_node(tree, &iter, new_pos);
	}
}

void for_tree_do(struct tree_t * tree,
	void (*fn)(struct node_t *, void *param),
	void * param)
{
	struct node_t start;
	ptr_type
		left = tree_leftmost(tree);
	if(left != 0)
	{
		tree_load_node(tree, &start, left);
		//tree_load_node(tree, &end, right);
		for_range_do(&start, NULL, fn, param);
	}
}
