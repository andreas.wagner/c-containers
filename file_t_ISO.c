#include "file_t.h"
#include "buffer.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>

#include <errno.h>

#include <unistd.h>
#include <sys/types.h>
#include <string.h>





struct file_t * open_file_ISO(
	char *filename,
	struct buffer_manager_t * mgr)
{
	struct file_t *my_file = malloc(sizeof(struct file_t));
	my_file->iso.fp = fopen(filename, "wb");
	fseek(my_file->iso.fp, 0, SEEK_END);
	my_file->size = ftell(my_file->iso.fp);
	my_file->owner = mgr;
	return my_file;
}

void close_file_ISO(
	struct file_t *file)
{
	fclose(file->iso.fp);
	free(file);
}


struct buffer_t * fetch_data_ISO(
	struct file_t *my_file,
	ptr_type pos,
	ptr_type size)
{
	if(pos+size > my_file->size)
		return NULL;
	struct buffer_t * return_value = malloc(sizeof(struct buffer_t));
	return_value->size = size;
	return_value->position = pos;
	return_value->data = malloc(size);
	return_value->owner = my_file->owner;
	fseek(my_file->iso.fp, pos, SEEK_SET);
	
	int ret = fread(
		((char*)return_value->data),
		size,
		1,
		my_file->iso.fp
		);
	if(likely(ret > 0))
	{
	}
	else
	{
		//TODO
		printf("read -1");
		if(errno != EAGAIN)
			abort();
	}
	return return_value;
}

void put_data_ISO (
	struct file_t * my_file,
	struct buffer_t * buffer)
{
	fseek(my_file->iso.fp, buffer->position, SEEK_SET);
	int ret = fwrite(
		((char *)buffer->data),
		buffer->size,
		1,
		my_file->iso.fp);
	if(likely(ret > 0))
	{
	}
	else
	{
		//TODO
		printf("write -1:-> %i ", buffer->position);
		if(errno != EAGAIN)
		{
			printf("%s", strerror(errno));
			abort();
		}
	}
}

struct buffer_t * allocate_space_ISO(
	struct file_t * my_file,
	ptr_type size)
{
	struct buffer_t * ret_val = malloc(sizeof(struct buffer_t));
	ret_val->data = malloc(size);
	ret_val->position = my_file->size;
	ret_val->size = size;
	ret_val->owner = my_file->owner;
	memset(ret_val->data, 0, ret_val->size);

	fseek(my_file->iso.fp, 0, SEEK_END);

	int ret = fwrite(
		ret_val->data,
		ret_val->size,
		1,
		my_file->iso.fp
	);
	if(likely(ret > 0))
	{
	}
	else
	{
		//TODO
		exit(1);
	}

	my_file->size += size;
	//printf("size: %i\n", my_file->size);
	return ret_val;
}


void deallocate_space_ISO(struct file_t * file, struct buffer_t * buf)
{
	memset(buf->data, 0, buf->size);
	put_data_ISO(file, buf);
}

