#include "file_t.h"
#include "buffer.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>

#include <errno.h>

#include <unistd.h>
#include <sys/types.h>
#include <string.h>


struct file_t * open_file_POSIX(
	char *filename,
	struct buffer_manager_t * mgr)
{
	struct file_t *my_file = malloc(sizeof(struct file_t));
	my_file->posix.fd = open(filename, O_CREAT|O_RDWR, 0600);
	lseek(my_file->posix.fd, 0, SEEK_END);
	struct stat sb;
	fstat(my_file->posix.fd, &sb);
	my_file->size = sb.st_size;
	my_file->owner = mgr;
	return my_file;
}

void close_file_POSIX(
	struct file_t *file)
{
	close(file->posix.fd);
	free(file);
}


struct buffer_t * fetch_data_POSIX(
	struct file_t *my_file,
	ptr_type pos,
	ptr_type size)
{
	if(pos+size > my_file->size)
		return NULL;
	struct buffer_t * return_value = malloc(sizeof(struct buffer_t));
	return_value->size = size;
	return_value->position = pos;
	return_value->data = malloc(size);
	return_value->owner = my_file->owner;
	ptr_type progress = 0;
	lseek(my_file->posix.fd, pos, SEEK_SET);
	while(progress < size)
	{
		int delta = read(
			my_file->posix.fd,
			((char*)return_value->data) + progress,
			size - progress);
		if(likely(delta > 0))
		{
			progress += delta;
		}
		else
		{
			//TODO
			printf("read -1");
			if(errno != EAGAIN)
				exit(1);
		}
	}
	return return_value;
}

void put_data_POSIX (
	struct file_t * my_file,
	struct buffer_t * buffer)
{
	ptr_type progress = 0;
	lseek(my_file->posix.fd, buffer->position, SEEK_SET);
	while(progress < buffer->size)
	{
		int delta = write(
			my_file->posix.fd,
			((char *)buffer->data) + progress,
			buffer->size - progress);
		if(likely(delta > 0))
		{
			progress += delta;
		}
		else
		{
			//TODO
			printf("write -1: %p -> %i ", buffer->data + progress, buffer->position);
			if(errno != EAGAIN)
			{
				printf("%s", strerror(errno));
				abort();
			}
		}
	}
}

struct buffer_t * allocate_space_POSIX(
	struct file_t * my_file,
	ptr_type size)
{
	struct buffer_t * ret_val = malloc(sizeof(struct buffer_t));
	ret_val->data = malloc(size);
	ret_val->position = my_file->size;
	ret_val->size = size;
	ret_val->owner = my_file->owner;
	memset(ret_val->data, 0, ret_val->size);

	ptr_type progress = 0;
	lseek(my_file->posix.fd, 0, SEEK_END);
	while(progress < ret_val->size)
	{
		int delta = write(
			my_file->posix.fd,
			ret_val->data + progress,
			ret_val->size - progress);
		if(likely(delta > 0))
		{
			progress += delta;
		}
		else
		{
			//TODO
			exit(1);
		}
	}

	my_file->size += size;
	//printf("size: %i\n", my_file->size);
	return ret_val;
}


void deallocate_space_POSIX(struct file_t * file, struct buffer_t * buf)
{
	memset(buf->data, 0, buf->size);
	put_data_POSIX(file, buf);
}
