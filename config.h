#ifndef _config_h

#include <stdint.h>

#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)

#define mem_alloc_size 1024*1024*64

#define ptr_type uint32_t
#define ptr_type_enum val_uint32

#endif

