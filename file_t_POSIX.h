
struct file_t * open_file_POSIX(
	char *filename, struct buffer_manager_t * mgr);

void close_file_POSIX(
	struct file_t *file);

struct buffer_t * fetch_data_POSIX(
	struct file_t *my_file,
	ptr_type pos,
	ptr_type size);
	
void put_data_POSIX(
	struct file_t * my_file,
	struct buffer_t * buffer);

struct buffer_t * allocate_space_POSIX(
	struct file_t * my_file,
	ptr_type size);
	
void deallocate_space_POSIX(struct file_t * file, struct buffer_t * buf);
