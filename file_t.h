#ifndef _file_t_h
#define _file_t_h

#include "config.h"
#include "buffer.h"
#include <stdio.h>

struct file_t
{
	union
	{
		struct 
		{
			int fd;
		} posix;
		struct
		{
			void * main_ptr;
			ptr_type allocated_space;
		} mem;
		struct
		{
			FILE * fp;
		} iso;
	};
	ptr_type size;
	struct buffer_manager_t * owner;
};

#include "file_t_ISO.h"
#include "file_t_POSIX.h"
#include "file_t_MEM.h"
#include "file_t_MEM_MANAGED.h"

#endif

