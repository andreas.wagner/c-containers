#!/bin/bash

rm -r build
mkdir build
cd build
if [ $1 == "-r" ]
then
	cmake -DCMAKE_BUILD_TYPE=Release ..
else
	cmake -DCMAKE_BUILD_TYPE=Debug ..
fi
rm *.pdf *.dot test.dat
make
if [ $1 == "-v" ]
then
#	valgrind --leak-check=full -s ./test_lib
	valgrind --leak-check=full ./test_lib
elif [ $1 == "-d" ]
then
	gdb ./test_lib
else
	./test_lib
fi
for i in *.dot
do
	dot -Tpdf $i -o $i.pdf
done
