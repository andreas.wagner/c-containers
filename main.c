#include <stdio.h>
#include <unistd.h>
#include <malloc.h>
#include <stdint.h>
#include <stdbool.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

#include "containers.h"

#define test_count 100

int counter = 0;

void count(struct node_t * node, void * param)
{
	(*(int *)param)++;
}

int main(int argc, char *argv[])
{
	umask(~0660);
	struct access_manager_t posix_man;
	init_access_manager(&posix_man, AM_POSIX);
	struct access_manager_t mem_man;
	init_access_manager(&mem_man, AM_MEM);
	
	struct buffer_manager_t tree_bm;
	init_buffer_manager(&tree_bm, &posix_man, "test.dat", &mem_man);
	
	
	struct tree_t tree;
	create_tree(&tree, 8, val_uint32, NULL, val_uint32, NULL, &tree_bm);
	
	tree_set_root_pos(&tree, 0);
	printf("inserting\n");
	srand(1);
	for(int i = 0; i < test_count; i++)
	{
		uint32_t key = rand(), value = 0;
		printf("\n--insert %i--\nkey: %x\n", i, key);
		fflush(stdout);
		tree_insert_key_value(&tree, &key, &value);
		//dump_tree(&tree, i);
		test_avl(&tree);
	}
	dump_tree(&tree, 99);
	printf("counting\n");
	for_tree_do(&tree, count, &counter);
	printf("%i counted\n", counter);
	counter = 0;
	//printf("\nflushing\n");
	//tree_flush(&tree);
	//printf("\nflushed\n");
	//printf("counting\n");
	//for_tree_do(&tree);
	
	printf("looking up\n");
	uint32_t * val;
	srand(1);
	for(int i = 0; i < test_count; i++)
	{
		uint32_t key = rand();
		//printf("\n--read %i--\nkey: %x\n", i, key);
		tree_get_value_for_key(&tree, (void *)&key, (void *) &val);
		//	printf("value:%x\n", *val);
		//fflush(stdout);
		free(val);
		test_avl(&tree);
	}
	printf("counting\n");
	for_tree_do(&tree, count, &counter);
	printf("%i counted\n", counter);
	counter = 0;


	printf("deleting\n");
	srand(1);
	for(int i = 0; i < test_count; i++)
	{
		uint32_t key = rand();
		printf("--remove %i--\nkey: %x  --  ", i, key);
		if(tree_remove_key(&tree, (void*) &key))
		{
			printf("delete ok\n");
		}
		else
			printf("delete failed\n");
		fflush(stdout);
		dump_tree(&tree, 100+i);
		test_avl(&tree);
		printf("counting\n");
		for_tree_do(&tree, count, &counter);
		printf("%i counted\n", counter);
		if(counter != test_count - i-1)
		{
			printf("FAIL\n");
		}
		counter = 0;
	}
	printf("counting\n");
	for_tree_do(&tree, count, &counter);
	printf("%i counted\n", counter);
	counter = 0;
	
	printf("\nflushing\n");
	tree_flush(&tree);
	printf("\nflushed\n");
	printf("counting\n");
	for_tree_do(&tree, count, &counter);
	printf("%i counted\n", counter);
	counter = 0;
//	delete_tree();
	delete_buffer_manager(&tree_bm, &tree);
	
	

	return 0;
}
