#include "file_t.h"
#include "buffer.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>

#include <errno.h>

#include <unistd.h>
#include <sys/types.h>
#include <string.h>




struct file_t * open_file_MEM_MANAGED(
	char *filename,
	struct buffer_manager_t * mgr)
{
	struct file_t *my_file = malloc(sizeof(struct file_t));
	int size = mem_alloc_size;
	my_file->mem.main_ptr = malloc(size);
	my_file->mem.allocated_space = size;
	my_file->size = 8; // TODO: reconsider; never use 0 here! Root-node would be placed there.
	my_file->owner = mgr;
	memset(my_file->mem.main_ptr, 0, my_file->mem.allocated_space);
	return my_file;
}

void close_file_MEM_MANAGED(
	struct file_t *file)
{
	free(file->mem.main_ptr);
	free(file);
}


struct buffer_t * fetch_data_MEM_MANAGED(
	struct file_t *my_file,
	ptr_type pos,
	ptr_type size)
{
	if(pos+size > my_file->size)
		return NULL;
	struct buffer_t * return_value = malloc(sizeof(struct buffer_t));
	return_value->position = pos;
	return_value->size = size;
	return_value->data = (void *)((char*)my_file->mem.main_ptr + pos);
	return_value->owner = my_file->owner;
	return return_value;
}

void put_data_MEM_MANAGED (
	struct file_t * my_file,
	struct buffer_t * buffer)
{
	// Nothing to do, the buffer maps a region of the allocated
	// memory in my_file->main_ptr.
}


// Warning outdated. Allocate a sufficient amount of memory for your work!
struct buffer_t * allocate_space_MEM_MANAGED(
	struct file_t * my_file,
	ptr_type size)
{
	if(my_file->mem.allocated_space < my_file->size + size)
	{
/*
		my_file->mem.allocated_space += 1024*1024;
		my_file->mem.main_ptr = realloc(my_file->mem.main_ptr, my_file->mem.allocated_space);
*/
		printf("Did not allocate sufficient memory for work.");
		abort();
	}
	struct buffer_t * ret_val = malloc(sizeof(struct buffer_t));
	ret_val->position = my_file->size;
	ret_val->data = (void *) ((char*)my_file->mem.main_ptr + ret_val->position); // would be invalid after realloc(), so no reallocs, please!
	ret_val->size = size;
	ret_val->owner = my_file->owner;
	memset(ret_val->data, 0, ret_val->size);

	my_file->size += size;
	return ret_val;
}


void deallocate_space_MEM_MANAGED(struct file_t * file, struct buffer_t * buf)
{
}
