#include "buffer.h"
#include <malloc.h>

struct buffer_excerpt_t *create_buffer_excerpt(
	struct buffer_t *origin,
	ptr_type pos,
	ptr_type size)
{
	struct buffer_excerpt_t * ret = malloc(sizeof(struct buffer_excerpt_t));
	ret->origin = origin;
	ret->data = origin->data + pos;
	ret->size = size;
	return ret;
}

void delete_buffer_excerpt(struct buffer_excerpt_t *be)
{
	free(be);
}
