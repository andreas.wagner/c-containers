#ifndef _access_manager_h
#define _access_manager_h

#include "config.h"
#include "containers.h"

//struct buffer_manager_t;

enum am_type
{
	AM_NONE,
	AM_MEM,
	AM_POSIX,
	AM_MMAP,
	AM_MEM_MANAGED,
	AM_INVALID
};

struct access_manager_t
{
	struct file_t * (*open_file)(
		char *filename,
		struct buffer_manager_t * mgr);
	void (*close_file)(
		struct file_t *file);
	struct buffer_t *(*fetch_data) (
		struct file_t * my_file,
		ptr_type posistion,
		ptr_type size);
	void (*put_data) (
		struct file_t * my_file,
		struct buffer_t * buffer);
	struct buffer_t * (*allocate_space)(
		struct file_t * my_file,
		ptr_type size);
	void (*deallocate_space)(
		struct file_t * file,
		struct buffer_t * buf);
	void (*free_buffer)(struct buffer_t * buf); //	remove from cache
	void (*release_buffer)(struct buffer_t * buf); // call free in AM_MEM; no cacheing used
	enum am_type type;
};

void init_access_manager(struct access_manager_t * am, enum am_type type);

#endif

