#include "my_types.h"
#include <malloc.h>
#include <stdlib.h>

int uint64_compare(struct buffer_excerpt_t *b1, struct buffer_excerpt_t *b2, void *)
{
	if(*((uint64_t*)(b1->data)) < *((uint64_t*)(b2->data)))
		return -1;
	else if(*((uint64_t*)(b1->data)) > *((uint64_t*)(b2->data)))
		return 1;
	else
		return 0;
}

void uint64_inject(struct buffer_excerpt_t * buf, void * value, void *)
{
	if(sizeof(uint64_t) == buf->size)
		*((uint64_t*)(buf->data)) = *((uint64_t*)(value));
	else
		// TODO
		abort();
		;
}

void uint64_extract(struct buffer_excerpt_t * buf, void * value, void *)
{
	if(sizeof(uint64_t) == buf->size)
		*((uint64_t*)(value)) = *((uint64_t*)(buf->data));
	else
		// TODO
		abort();
		;
}

void * uint64_alloc(struct buffer_excerpt_t * buf, void *)
{
	return malloc(sizeof(uint64_t));
}


int uint32_compare(struct buffer_excerpt_t *b1, struct buffer_excerpt_t *b2, void *)
{
	//printf("comparing %x and %x\n", *(uint32_t*)(b1->data),  *(uint32_t*)(b2->data));
	if(*(uint32_t*)(b1->data) < *(uint32_t*)(b2->data))
		return -1;
	else if(*((uint32_t*)(b1->data)) > *((uint32_t*)(b2->data)))
		return 1;
	else
		return 0;
}

void uint32_inject(struct buffer_excerpt_t * buf, void * value, void *)
{
	//printf("injecting; size %i, data: %i\n", buf->size, *((uint32_t*)value));
	if(sizeof(uint32_t) == buf->size)
		*(uint32_t*)(buf->data) = *((uint32_t*)value);
	else
		// TODO
		abort();
		;
}

void uint32_extract(struct buffer_excerpt_t * buf, void * value, void *)
{
	//printf("extracting; size %i, data: %i\n", buf->size, *((uint32_t*)value));
	if(sizeof(uint32_t) == buf->size)
		*(uint32_t*)(value) = *((uint32_t*)buf->data);
	else
		// TODO
		abort();
		;
}

void * uint32_alloc(struct buffer_excerpt_t * buf, void *)
{
	return malloc(sizeof(uint32_t));
}



int int8_compare(struct buffer_excerpt_t *b1, struct buffer_excerpt_t *b2, void *)
{
	if(*(int8_t*)(b1->data) < *(int8_t*)(b2->data))
		return -1;
	else if(*(int8_t*)(b1->data) > *(int8_t*)(b2->data))
		return 1;
	else
		return 0;
}

void int8_inject(struct buffer_excerpt_t * buf, void * value, void *)
{
	if(sizeof(int8_t) == buf->size)
		*(int8_t*)(buf->data) = *(int8_t*)(value);
	else
		// TODO
		abort();
		;
}

void int8_extract(struct buffer_excerpt_t * buf, void * value, void *)
{
	if(sizeof(int8_t) == buf->size)
		*(int8_t*)(value) = *(int8_t*)(buf->data);
	else
		// TODO
		abort();
		;
}

void * int8_alloc(struct buffer_excerpt_t * buf, void *)
{
	return malloc(sizeof(int8_t));
}



int null_compare(struct buffer_excerpt_t *, struct buffer_excerpt_t *, void *)
{
		return 0;
}

void null_inject(struct buffer_excerpt_t * , void * , void *)
{
}

void null_extract(struct buffer_excerpt_t * , void * , void *)
{
}

void * null_alloc(struct buffer_excerpt_t * , void *)
{
	return NULL;
}

