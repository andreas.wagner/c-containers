#ifndef _containers_h
#define _containers_h

#include <stdio.h>
#include <unistd.h>
#include <malloc.h>
#include <stdint.h>
#include <stdbool.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

#include "buffer.h"
#include "file_t.h"
#include "access_manager.h"
#include "my_types.h"



enum val_type
{
	val_null,
	val_uint32,
	val_uint64
};


struct buffer_manager_t;

struct tree_t
{
	ptr_type pos_of_root_pos;
	struct buffer_manager_t * buff_manager;
	ptr_type sizeof_key;
	ptr_type sizeof_value;
	void * key_attrib;
	void * value_attrib;
	void (*ptr_decode)(struct buffer_excerpt_t * be, void *p, void * attrib);
	void (*ptr_encode)(struct buffer_excerpt_t * be, void *p, void * attrib);
	int (*key_compare)(struct buffer_excerpt_t * key1, struct buffer_excerpt_t *key2, void * key_attrib);
	void (*key_extract)(struct buffer_excerpt_t * buf, void * key, void * key_attrib);
	void (*key_inject)(struct buffer_excerpt_t * buf, void * key, void * key_attrib);
	void * (*key_alloc)(struct buffer_excerpt_t * buf, void * key_attrib);
	void (*value_extract)(struct buffer_excerpt_t * buf, void * value, void * value_attrib);
	void (*value_inject)(struct buffer_excerpt_t * buf, void * value, void * value_attrib);
	void * (*value_alloc)(struct buffer_excerpt_t * buf, void * value_attrib);
};


struct node_t
{
	struct tree_t * owner;
	struct buffer_t * buffer;
};


struct buffer_manager_t
{
	struct access_manager_t * access_manager;
	struct file_t * storage;
	struct tree_t * clean_nodes;
	struct tree_t * dirty_nodes;
};



#include "containers.h"



bool tree_get_value_for_key(struct tree_t *tree, void * key, void ** value);

struct buffer_t * fetch_buffer(struct buffer_manager_t * bm, ptr_type pos, ptr_type size);

// TODO: think about when to remove from clean_nodes or dirty_nodes!
void free_buffer(struct buffer_t * buf);

void tree_set_key_type(struct tree_t * tree, enum val_type t, void * attrib);

void tree_set_value_type(struct tree_t * tree, enum val_type t, void * attrib);

struct buffer_t * tree_fetch_node_buffer(struct tree_t * tree, ptr_type pos);

ptr_type tree_get_root_pos(struct tree_t * tree);

void tree_set_root_pos(struct tree_t * tree, ptr_type pos);

void tree_get_root_node(struct tree_t * tree, struct node_t * ret_node);

bool tree_remove_key(struct tree_t * tree, void* data);

bool tree_insert_key_value(struct tree_t * tree, void* key, void* value);

void tree_flush(struct tree_t * tree);

void mark_dirty(struct buffer_t* buff);

void init_buffer_manager(
	struct buffer_manager_t * mgr,
	struct access_manager_t * am_tree,
	char * filename,
	struct access_manager_t * am_mem);

void delete_buffer_manager(
	struct buffer_manager_t *mgr,
	struct tree_t *mgd_tree);

void create_tree(
	struct tree_t * tree,
	ptr_type pos,
	enum val_type key_t, void * key_attrib,
	enum val_type val_t, void * val_attrib,
	struct buffer_manager_t * bm_file);

void delete_tree(struct tree_t * tree);



void for_range_do(struct node_t * start,
	struct node_t * end,
	void (*fn)(struct node_t *, void *param),
	void * param);
void for_tree_do(struct tree_t * tree,
	void (*fn)(struct node_t *, void *param),
	void * param);


ptr_type tree_get_parent_pos(struct node_t * source_node);

ptr_type tree_get_right_pos(struct node_t * source_node);

ptr_type tree_get_left_pos(struct node_t * source_node);

int8_t tree_get_node_balance(struct node_t * source_node);

void tree_set_parent_pos(struct node_t * source_node, ptr_type pos);

void tree_set_right_pos(struct node_t * source_node, ptr_type pos);

void tree_set_left_pos(struct node_t * source_node, ptr_type pos);

void tree_set_node_balance(struct node_t * source_node, int8_t balance);

void tree_get_node_key(struct node_t * source_node, void ** key);

void tree_get_node_value(struct node_t * source_node, void ** value);



void dump_tree(struct tree_t * tree, int n);

void test_avl(struct tree_t * tree);
#endif
