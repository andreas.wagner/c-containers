#ifndef _my_types_h
#define _my_types_h

#include "buffer.h"

extern int uint64_compare(struct buffer_excerpt_t *b1, struct buffer_excerpt_t *b2, void * attrib);

extern void uint64_inject(struct buffer_excerpt_t * buf, void * value, void * attrib);

extern void uint64_extract(struct buffer_excerpt_t * buf, void * value, void * attrib);

extern void * uint64_alloc(struct buffer_excerpt_t * buf, void * attrib);


extern int uint32_compare(struct buffer_excerpt_t *b1, struct buffer_excerpt_t *b2, void * attrib);

extern void uint32_inject(struct buffer_excerpt_t * buf, void * value, void * attrib);

extern void uint32_extract(struct buffer_excerpt_t * buf, void * value, void * attrib);

extern void * uint32_alloc(struct buffer_excerpt_t * buf, void * attrib);


extern int int8_compare(struct buffer_excerpt_t *b1, struct buffer_excerpt_t *b2, void * attrib);

extern void int8_inject(struct buffer_excerpt_t * buf, void * value, void * attrib);

extern void int8_extract(struct buffer_excerpt_t * buf, void * value, void * attrib);

extern void * int8_alloc(struct buffer_excerpt_t * buf, void * attrib);


extern int null_compare(struct buffer_excerpt_t *b1, struct buffer_excerpt_t *b2, void *);

extern void null_inject(struct buffer_excerpt_t * buf, void * value, void *);

extern void null_extract(struct buffer_excerpt_t * buf, void * value, void *);

extern void * null_alloc(struct buffer_excerpt_t * buf, void *);


#endif
