#ifndef _BUFFER_H_
#define _BUFFER_H_

#include "config.h"
#include <stdbool.h>

struct buffer_manager_t;

struct buffer_t
{
	struct buffer_manager_t * owner;
	ptr_type size;
	ptr_type position;
	char * data;
};

struct buffer_excerpt_t
{
	struct buffer_t * origin;
	ptr_type size;
	char * data;	
};


struct buffer_excerpt_t *create_buffer_excerpt(
	struct buffer_t *origin,
	ptr_type pos,
	ptr_type size);

void delete_buffer_excerpt(struct buffer_excerpt_t *be);


#endif

